//import logo from './logo.svg';
//import './App.css';
import Nav from './Nav'

import AttendeesList from './AttendeesList';

import LocationForm from './LocationForm';

import ConferenceForm from './ConferenceForm'

import PresentationForm from './PresentationForm'

import { BrowserRouter, Routes, Route } from 'react-router-dom';

import AttendConferenceForm from './AttendConferenceForm';
import MainPage from './MainPage';



function App(props) {
  if (props.attendees === undefined) {
    return null
  }
  
  return (
    <BrowserRouter>
      <Nav />
      {/* <div className='container'> */}
        <Routes>
          //MainPage Route
          <Route index element={<MainPage />} />
          //LocationForm Route
          <Route path='locations'>
            <Route path='new' element={<LocationForm />} />
          </Route>
          //ConferenceForm Route
          <Route path='conferences'>
            <Route path='new' element={<ConferenceForm />}/>
          </Route>
          //PresentationForm Route
          <Route path='presentations'>
            <Route path='new' element={<PresentationForm />} />
          </Route>
          //AttendeesList Route
          <Route path='attendees'>
            <Route path='' element={<AttendeesList attendees={props.attendees} />} />
            <Route path='new' element={<AttendConferenceForm />} />
          </Route>

          
        </Routes>
          {/* <ConferenceForm /> */}
          {/* <LocationForm /> */}
          {/*<AttendeesList attendees={props.attendees} />*/}
      {/* </div> */}
    </BrowserRouter>
  );
}

export default App;
