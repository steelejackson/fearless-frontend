import React, { useEffect, useState } from 'react'

function PresentationForm() {

    //hooks and state management
    

    //set the useState hook to store 'name' in the component's state,
    //with the default initial value of an empty string.
    const [presenterName, setName] = useState('')

    const [presenterEmail, setEmail] = useState('')

    const [companyName, setCompanyName] = useState('')

    const [title, setTitle] = useState('')

    const [synopsis, setSynopsis] = useState('')

    //set state of conferenceList

    const [conferences, setConferences] = useState([])

    //set state of selected conference

    const [conference, setConference] = useState('')

    

    //create the handleNameChange method to take what the user inputs
    //into the form and store it in the state's 'name' variable

    const handleNameChange = (event) => {
        const value = event.target.value
        setName(value)
    }

    const handleEmailChange = (event) => {
        const value = event.target.value
        setEmail(value)
    }

    const handleCompanyNameChange = (event) => {
        const value = event.target.value
        setCompanyName(value)

    }

    const handleTitleChange = (event) => {
        const value = event.target.value
        setTitle(value)
    }

    const handleSynopsisChange = (event) => {
        const value = event.target.value
        setSynopsis(value)
    }

    const handleConferenceChange = (event) => {
        const value = event.target.value
        setConference(value)
    }


    //handleSubmit

    const handleSubmit = async (event) => {
        event.preventDefault()

        //create an empty JSON object

        const data = {}

        data.presenter_name = presenterName
        data.company_name = companyName
        data.presenter_email = presenterEmail
        data.title = title
        data.synopsis = synopsis
        data.conference = conference
        

        console.log(data)

        console.log(data.conference)

        

        

        

        const presentationUrl = `http://localhost:8000${data.conference}presentations/`

        

        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
              'Content-Type': 'application/json',
            },
        };

        const response = await fetch(presentationUrl, fetchConfig)

        console.log(response)
        if (response.ok) {
            const newPresentation = await response.json()
            console.log(newPresentation)

            setName('')
            setEmail('')
            setCompanyName('')
            setTitle('')
            setSynopsis('')
            setConference('')
        }
    }



    //fetch conferences

    const fetchData = async () => {
        const url = 'http://localhost:8000/api/conferences/'

        const response = await fetch(url)

        if (response.ok) {
            const data = await response.json()

            console.log(data)
            setConferences(data.conferences)

            
            
            
        
        }

    }

    useEffect(() => {
        fetchData()
    }, [])

    

  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
return (

    <div>
      {/* <header>
        <nav className="navbar navbar-expand-lg navbar-light bg-light">
          <div className="container-fluid">
            <a className="navbar-brand" href="/">Conference GO!</a>
            <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
              <span className="navbar-toggler-icon"></span>
            </button>
            <div className="collapse navbar-collapse" id="navbarSupportedContent">
              <ul className="navbar-nav me-auto mb-2 mb-lg-0">
                <li className="nav-item">
                  <a className="nav-link" aria-current="page" href="#">Home</a>
                </li>
                <li className="nav-item">
                  <a className="nav-link d-none" aria-current="page" href="new-location.html">New location</a>
                </li>
                <li className="nav-item">
                  <a className="nav-link d-none" aria-current="page" href="new-conference.html">New conference</a>
                </li>
                <li className="nav-item">
                  <a className="nav-link active" aria-current="page" href="new-presentation.html">New presentation</a>
                </li>
              </ul>
              <form className="d-flex">
                <input className="form-control me-2" type="search" placeholder="Search conferences" aria-label="Search" />
                <button className="btn btn-outline-success" type="submit">Search</button>
                <a className="btn btn-primary" href="attend-conference.html">Attend!</a>
              </form>
            </div>
          </div>
        </nav>
      </header> */}
      <main>
        <div className="container">
          <div className="row">
            <div className="offset-3 col-6">
              <div className="shadow p-4 mt-4">
                <h1>Create a new presentation</h1>
                <form onSubmit={handleSubmit} id="create-presentation-form">
                  <div className="form-floating mb-3">
                    <input onChange={handleNameChange} placeholder="Presenter name" required type="text" name="presenter_name" value={presenterName} id="presenter_name" className="form-control" />
                    <label htmlFor="presenter_name" >Presenter name</label>
                  </div>
                  <div className="form-floating mb-3">
                    <input onChange={handleEmailChange} placeholder="Presenter email" required type="email" name="presenter_email" value={presenterEmail} id="presenter_email" className="form-control" />
                    <label htmlFor="presenter_email">Presenter email</label>
                  </div>
                  <div className="form-floating mb-3">
                    <input onChange={handleCompanyNameChange} placeholder="Company name" type="text" name="company_name" id="company_name" value={companyName} className="form-control" />
                    <label htmlFor="company_name">Company name</label>
                  </div>
                  <div className="form-floating mb-3">
                    <input onChange={handleTitleChange} placeholder="Title" required type="text" name="title" id="title" value={title} className="form-control" />
                    <label htmlFor="title">Title</label>
                  </div>
                  <div className="mb-3">
                    <label htmlFor="synopsis">Synopsis</label>
                    <textarea onChange={handleSynopsisChange} id="synopsis" value={synopsis} rows="3" name="synopsis" className="form-control"></textarea>
                  </div>
                  <div className="mb-3">
                    <select onChange={handleConferenceChange} required id="conference" name="conference"  value={conference} className="form-select">
                      <option value="">Choose a conference</option>
                      {conferences.map(conference => {
                        return (
                            <option key={conference.href} value={conference.href}>
                                {conference.name}
                            </option>
                        )
                    })}

            


                    </select>
                  </div>
                  <button className="btn btn-primary">Create</button>
                </form>
              </div>
            </div>
          </div>
        </div>
      </main>
    </div>
  );
}





export default PresentationForm



