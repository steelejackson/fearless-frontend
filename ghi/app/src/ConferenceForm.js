import React, {useEffect, useState } from 'react'

function ConferenceForm() {

    //hooks

    
    const [name, setName] = useState('')

    const [starts, setStarts] = useState('')

    const [ends, setEnds] = useState('')

    const [description, setDescription] = useState('')

    const [maxPresentations, setMaxPresentations] = useState('')

    const [maxAttendees, setMaxAttendees] = useState('')

    const [location, setLocation] = useState('')

    const [locations, setLocations] = useState([])

    
    //handleChange functions

    const handleNameChange = (event) => {
        const value = event.target.value
        setName(value)
    }

    const handleStartChange = (event) => {
        const value = event.target.value
        setStarts(value)
    }

    const handleEndChange = (event) => {
        const value = event.target.value
        setEnds(value)
    }

    const handleDescriptionChange = (event) => {
        const value = event.target.value
        setDescription(value)
    }

    const handleMaxPresentationChange = (event) => {
        const value = event.target.value
        setMaxPresentations(value)
    }

    const handleMaxAttendeesChange = (event) => {
        const value = event.target.value
        setMaxAttendees(value)
    }

    const handleLocationChange = (event) => {
        const value = event.target.value
        setLocation(value)
    }

    //handleSubmit function

    const handleSubmit = async (event) => {
        event.preventDefault()

        const data = {}

        data.name = name
        data.starts = starts
        data.ends = ends
        data.description = description
        data.max_presentations = maxPresentations
        data.max_attendees = maxAttendees
        data.location = location

        const conferenceUrl = 'http://localhost:8000/api/conferences/'

        const fetchConfig = {
            method: 'post',
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json'
            }

        }

        const response = await fetch(conferenceUrl, fetchConfig)
        if (response.ok) {
            const newConference = await response.json()

            console.log(newConference)

            setName('')
            setStarts('')
            setEnds('')
            setDescription('')
            setMaxPresentations('')
            setMaxAttendees('')
            setLocation('')
            
        }

    }

    //location data

    const fetchData = async () => {
        const url = 'http://localhost:8000/api/locations/'

        const response = await fetch(url)

        if (response.ok) {
            const data = await response.json()

            console.log('API response data:', data.locations)
            
            setLocations(data.locations)
        }
    }

    //useEffect hook that triggers the fetchData function when the component mounts. this renders the list of locations in the form.

    useEffect(() => {
        fetchData()
    }, [])


    console.log('Locations:', locations)

    return (
        <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Add a new conference</h1>
            <form onSubmit={handleSubmit} id="create-conference-form">
              <div className="form-floating mb-3">
                <input onChange={handleNameChange} placeholder="Name" required type="text" id="name" name='name' value={name} className="form-control"/>
                <label htmlFor="name">Name</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleStartChange} placeholder="Start date" required type="date" id="starts" name='starts' value={starts} className="form-control"/>
                <label htmlFor="starts">Start date</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleEndChange} placeholder="End date" required type="date" id="ends" name='ends' value={ends} className="form-control"/>
                <label htmlFor="ends">End date</label>
              </div>
              <div className="form-group mb-3">
                <textarea onChange={handleDescriptionChange} placeholder='Description' type='textarea' className="form-control" id="description" value={description} rows="5" style={{height: '100%'}} name="description"></textarea>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleMaxPresentationChange} placeholder="Maximum presentations" required type="number" id="max_presentations" name='max_presentations' value={maxPresentations}className="form-control"/>
                <label htmlFor="max_presentations">Maximum presentations</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleMaxAttendeesChange} placeholder="Maximum attendees" required type="number" id="max_attendees" name='max_attendees' value={maxAttendees} className="form-control"/>
                <label htmlFor="max_attendees">Maximum attendees</label>
              </div>
              <div className="mb-3">
                <select onChange={handleLocationChange} required id="location" className="form-select" name='location' value={location}>
                  <option value="">Choose a location</option>


                  
                
                    
                  {locations.map(location => {
                    return (
                        <option key={location.id} value={location.id}>
                            {location.name}
                        </option>
                    )
                  })}

               
                




                </select>
              </div>
              <button className="btn btn-primary">Create</button>
            </form>
          </div>
        </div>
      </div>
    )
}

export default ConferenceForm