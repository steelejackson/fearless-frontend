import React, { useEffect, useState } from 'react'

function LocationForm (props) {

    const [states, setStates] = useState([])

    //set the useState hook to store 'name' in the component's state,
    //with the default initial value of an empty string.
    const [name, setName] = useState('')

    const [roomCount, setRoomCount] = useState('')

    const [city, setCity] = useState('')

    const [state, setState] = useState('')

    

    //create the handleNameChange method to take what the user inputs
    //into the form and store it in the state's 'name' variable

    const handleNameChange = (event) => {
        const value = event.target.value
        setName(value)
    }

    const handleRoomCountChange = (event) => {
        const value = event.target.value
        setRoomCount(value)

    }

    const handleCityChange = (event) => {
        const value = event.target.value
        setCity(value)
    }

    const handleStateChange = (event) => {
        const value = event.target.value
        setState(value)
    }


    const handleSubmit = async (event) => {
        event.preventDefault()

        //create an empty JSON object

        const data = {}

        data.room_count = roomCount
        data.name = name
        data.city = city
        data.state = state
        

        console.log(data)

        const locationUrl = 'http://localhost:8000/api/locations/'

        const fetchConfig = {
            method: 'post',
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json'
            }
        }

        const response = await fetch(locationUrl, fetchConfig)
        if (response.ok) {
            const newLocation = await response.json()
            console.log(newLocation)

            setName('')
            setRoomCount('')
            setCity('')
            setState('')
        }
    }



    //The event parameter is the event that occurred. The target property is the HTML tag that caused the event. So, in this case, event.target is the user's input in the form for the location's name. The event.target.value property is the text that the user typed into the form.



    const fetchData = async () => {
        const url = 'http://localhost:8000/api/states/'

        const response = await fetch(url)

        if (response.ok) {
            const data = await response.json()

            console.log(data)
            setStates(data)
            
            
        {/*}
            console.log(data)
            
            const selectTag = document.getElementById('state')

            for (let state of data) {
                const option = document.createElement('option')
                option.value = state.abbreviation
                option.innerHTML = state.name
                selectTag.appendChild(option)

            }
        */}
        }

    }

    useEffect(() => {
        fetchData()
    }, [])

    








    return (
        <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Create a new location</h1>
            <form onSubmit={handleSubmit} id="create-location-form">
              <div className="form-floating mb-3">
                <input onChange={handleNameChange} placeholder="Name" required type="text" id="name" name='name' value={name} className="form-control"/>
                <label htmlFor="name">Name</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleRoomCountChange} placeholder="Room count" required type="number" id="room_count" name='room_count' value={roomCount} className="form-control"/>
                <label htmlFor="room_count">Room count</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleCityChange} placeholder="City" required type="text" id="city" name='city' value={city} className="form-control"/>
                <label htmlFor="city">City</label>
              </div>
              <div className="mb-3">
                <select onChange={handleStateChange} required id="state" className="form-select" value={state} name='state'>
                  <option value="">Choose a state</option>

                  {states.map(state => {
                    return (
                        <option key={state.abbreviation} value={state.abbreviation}>
                            {state.name}
                        </option>
                    )
                  })}
                </select>
              </div>
              <button className="btn btn-primary">Create</button>
            </form>
          </div>
        </div>
      </div>
    )
}

export default LocationForm