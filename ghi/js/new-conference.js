

window.addEventListener('DOMContentLoaded', async () => {

    const url = 'http://localhost:8000/api/locations/'

    const response = await fetch(url)

    try {
        if(!response.ok) {
            //add error handler here
        } else {

            const data = await response.json()

            console.log(data)
            

            const selectElement = document.querySelector('.form-select')


            for(let location of data.locations) {
                console.log(location)
                
                console.log(location.name)

                const optionElement = document.createElement('option')

                optionElement.value = location.id
                optionElement.text = location.name
                selectElement.appendChild(optionElement)

            }


            const formTag = document.getElementById('create-conference-form')
            formTag.addEventListener('submit', async(event) => {
                event.preventDefault()
                console.log('form submitted')


                const formData = new FormData(formTag)

               

                const json = JSON.stringify(Object.fromEntries(formData))

                

                const conferenceUrl = 'http://localhost:8000/api/conferences/'

                

                const fetchConfig = {
                    method: 'post',
                    body: json,
                    headers: {
                        'Content-Type': 'application/json',
                    },
                }
                const response = await fetch(conferenceUrl, fetchConfig)
                if (response.ok) {
                    formTag.reset()
                    const newConference = await response.json()

                    console.log('new conference created', newConference)
                    
                } else {
                    console.error('Error creating conference:', response.statusText);
        // Log response details for further investigation
                    console.error('Response status:', response.status);
                    console.error('Response headers:', response.headers);
                    console.error('Response text:', await response.text());
                }
                
            })








        }
    } catch (error) {
        // Handle fetch error here
        console.error('Error fetching data:', error)
    }

})


