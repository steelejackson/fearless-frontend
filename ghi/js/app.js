function createCard(name, description, pictureUrl, start, end, venue) {
    return `
    <div class="card shadow p-3 mb-5 bg-white rounded">
        <img src="${pictureUrl}" class="card-img-top img-fluid">
        <div class="card-body">
            <h5 class="card-title">${name}</h5>
            <h6 class="card-subtitle mb-2 text-muted">${venue}</h6>
            <p class="card-text">${description}</p>
        <div class="card-footer text-muted">${start} - ${end}
        </div>
    </div>`
}







window.addEventListener('DOMContentLoaded', async () => {

    const url  = 'http://localhost:8000/api/conferences/'

    const response = await fetch(url)

    try {
        const response = await fetch(url)

        if (!response.ok) {
            //figure out what to do when the response is bad

            var alertPlaceholder = document.getElementById('liveAlertPlaceholder')
            var alertTrigger = document.getElementById('liveAlertBtn')

            function alert(message, type) {
                var wrapper = document.createElement('div')
                wrapper.innerHTML = '<div class="alert alert-' + type + ' alert-dismissible" role="alert">' + message + '<button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button></div>'

                alertPlaceholder.append(wrapper)
            }

            if (alertTrigger) {
                alertTrigger.addEventListener('click', function () {
                    alert('bad response', 'warning')
                })
            }
        } else {
            const data = await response.json()



            


            for(let conference of data.conferences) {

                const detailUrl = `http://localhost:8000${conference.href}`

                const detailResponse = await fetch(detailUrl)

                if(detailResponse.ok) {

                    //this detail response is specifically for the name and description of a conference

                    const details = await detailResponse.json()
                    
                    const startDate = new Date(details.conference.starts).toLocaleDateString()

                    const endDate = new Date(details.conference.ends).toLocaleDateString()

                    const conferenceDescription = details.conference['description']
                    
                    const conferenceName = details.conference['name']

                    const conferenceImageUrl = details.conference.location['picture_url']

                    const venue = details.conference.location['name']

                    const row = document.querySelector('.row')

                    const html = createCard(conferenceName, conferenceDescription, conferenceImageUrl, startDate, endDate, venue)

                
                    row.innerHTML += html



                    

                }


            }
            




            

            

            
        }
    } catch (e) {
        //figure out what to do if an error is raised

        var alertPlaceholder = document.getElementById('liveAlertPlaceholder')
            var alertTrigger = document.getElementById('liveAlertBtn')

            function alert(message, type) {
                var wrapper = document.createElement('div')
                wrapper.innerHTML = '<div class="alert alert-' + type + ' alert-dismissible" role="alert">' + message + '<button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button></div>'

                alertPlaceholder.append(wrapper)
            }

            if (alertTrigger) {
                alertTrigger.addEventListener('click', function () {
                    alert('error', 'warning')
                })
            }
    }

    
});
